#!/bin/bash

# Handle self referencing, sourcing etc.
if [[ $0 != $BASH_SOURCE ]]; then
  export CMD=$BASH_SOURCE
else
  export CMD=$0
fi

# Ensure a consistent working directory so relative paths work.
pushd `dirname $CMD` > /dev/null
BASE=`pwd -P`
popd > /dev/null
cd $BASE

export GOPATH=$HOME/go/

# Cleanup any existing go directories.
go clean -modcache
rm -rf $GOPATH

# Fetch packer
go get github.com/hashicorp/packer && cd $GOPATH/src/github.com/hashicorp/packer

while pkill -0 2> /dev/null; do sleep 1; done;

# Fetch gox
go get github.com/mitchellh/gox && cd $GOPATH/src/github.com/mitchellh/gox
go build -o bin/gox .

